class Note{
  String noteID;
  String noteTitle;
  DateTime createdAt;
  DateTime updatedAt;

  Note({this.noteID,this.noteTitle,this.createdAt,this.updatedAt});

  factory Note.fromJson(Map<String,dynamic> item){
    return Note(
          noteID: item['noteID'],
          noteTitle: item['noteTitle'],
          createdAt: DateTime.parse(item['createDateTime']),
          updatedAt: item['latestEditDateTime'] != null 
                      ? DateTime.parse(item['latestEditDateTime']) 
                      : null
          );
  }
}