class SpecificNote{
  String noteID;
  String noteTitle;
  String noteContent;
  DateTime createdAt;
  DateTime updatedAt;

  SpecificNote({this.noteID,this.noteContent,this.noteTitle,this.createdAt,this.updatedAt});

  factory SpecificNote.fromJson(Map<String,dynamic> jsonData){
     return SpecificNote(
            noteID: jsonData['noteID'],
            noteTitle: jsonData['noteTitle'],
            noteContent: jsonData['noteContent'],
            createdAt: DateTime.parse(jsonData['createDateTime']),
            updatedAt: jsonData['latestEditDateTime'] != null 
                        ? 
                        DateTime.parse(jsonData['latestEditDateTime']) 
                        : null
            );
      }

  }