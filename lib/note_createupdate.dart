import 'package:HttpRequest_Apps/models/note_insert.dart';
import 'package:HttpRequest_Apps/services/notes_service.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

import 'models/specificnote.dart';
class NoteCreateUpdate extends StatefulWidget {
  final String noteID;
  //auto get define method
  //noteID get from notelist and use constructor to receive
  NoteCreateUpdate({this.noteID});

  _NoteCreateUpdate createState()=> _NoteCreateUpdate();
}

class _NoteCreateUpdate extends State<NoteCreateUpdate>{
  bool get isEditing => widget.noteID != null;
  NotesService get notesService => GetIt.I<NotesService>();

  String errorMessage;
  SpecificNote spNote;

  TextEditingController _titleController = TextEditingController();
  TextEditingController _contentController = TextEditingController();

  bool isloading = false;

  @override
  void initState() {
    super.initState();
   
    if(isEditing){
        setState((){
          isloading = true;
        });

          notesService.getSpecificNote(widget.noteID).then((response){
            setState((){
              isloading = false;
            });
            if(response.error){
              // ?? means if response.errorMessage is null then we created an error message 'An error occurred' use ??
              errorMessage = response.errorMessage ?? 'An error occurred';
            }
            spNote = response.data;
            //means that the api specific note data is saved into titlecontroller ,then the interface textfield there can read and see directly the data
            _titleController.text = spNote.noteTitle;
          });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.noteID == null ?"Create Note" : "Edit Note" ),
      ),
      body:Padding(
        padding:EdgeInsets.all(15),
        child:isloading ? Center(child:CircularProgressIndicator()) : Column(
          children: [
            TextField(
              //this controller help to connect 
              controller: _titleController,
              decoration: InputDecoration(
                hintText: "Title"
              ),
            ),

            Container(height: 25),
            
            TextField(
              controller:_contentController,
              decoration:InputDecoration(
                hintText:"Content"
              )
            ),

            Container(height: 25),
            
            SizedBox(
              //sizedBox with width 2 times infinity to make the button expanded full
              width:double.infinity,
              height:50,
              child:RaisedButton(
                child:Text("Submit",style: TextStyle(color: Colors.white)),
                onPressed: () async{
                  if(isEditing){
                    //update
                      setState((){
                      isloading=true;
                    });
                    final note = NoteInsert(
                      noteTitle: _titleController.text,
                      noteContent: _contentController.text  
                    );
                    final result = await notesService.updateNote(widget.noteID,note);
                    
                    setState((){
                      isloading = false;
                    });
                    
                    final successfulMessage ='Done';
                    final errorMessage = result.error ? (result.errorMessage ?? 'An error occured') : "Your note was updated";

                    showDialog(
                      context:context,
                      builder:(_) => AlertDialog(
                        title:Text(successfulMessage),
                        content:Text(errorMessage),
                        actions: [
                          FlatButton(
                            onPressed:(){
                              Navigator.of(context).pop();
                            }, 
                            child:Text("Ok")
                          )
                        ],
                      ) 
                    ).then((data){
                      if(result.data){
                        Navigator.of(context).pop();
                      }
                    });
                  }else{
                    //create
                    setState((){
                      isloading=true;
                    });
                    final note = NoteInsert(
                      noteTitle: _titleController.text,
                      noteContent: _contentController.text  
                    );
                    final result = await notesService.createNote(note);
                    
                    setState((){
                      isloading = false;
                    });
                    
                    final successfulMessage ='Done';
                    final errorMessage = result.error ? (result.errorMessage ?? 'An error occured') : "Your note was created";

                    showDialog(
                      context:context,
                      builder:(_) => AlertDialog(
                        title:Text(successfulMessage),
                        content:Text(errorMessage),
                        actions: [
                          FlatButton(
                            onPressed:(){
                              Navigator.of(context).pop();
                            }, 
                            child:Text("Ok")
                          )
                        ],
                      ) 
                    ).then((data){
                      if(result.data){
                        Navigator.of(context).pop();
                      }
                    });
                  }
                },
                color:Theme.of(context).primaryColor,
            ))
          ],
        ),
    ));
  }
}