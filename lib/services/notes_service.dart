import 'dart:convert';

import 'package:HttpRequest_Apps/models/api_response.dart';
import 'package:HttpRequest_Apps/models/note.dart';
import 'package:HttpRequest_Apps/models/note_insert.dart';
import 'package:HttpRequest_Apps/models/specificnote.dart';
import 'package:http/http.dart' as http;

class NotesService{
  //Map means object | List means array
  static const API = 'https://tq-notes-api-jkrgrdggbq-el.a.run.app/';
  static const headers = {
    'apiKey':"d078c6bb-18cc-499a-9266-3ac4268dcca3",
    'Content-Type':'application/json'
  };

  //List for get alls
  Future<APIResponse<List<Note>>> getNoteList(){
    return http.get(API+"/notes",headers:headers).then((data){
      if(data.statusCode == 200){
        //array json data here (jsonData)
        final jsonData = jsonDecode(data.body);
        //declare notes is array
        final notes = <Note>[];
        for(var item in jsonData){
          //cannot use push here due to push is not define inside the List
          notes.add(Note.fromJson(item));
        }
        return APIResponse<List<Note>>(data: notes);
      }
      return APIResponse<List<Note>>(error: true,errorMessage: "An error occured");
    }).catchError((error)=>{
      APIResponse<List<Note>>(error: true , errorMessage:error )
    });

    
    // List <Note> getNoteList(){
    // return[
    //   new Note(
    //     noteID: "1",
    //     noteTitle: "Note 1",
    //     createdAt: DateTime.now(),
    //     updatedAt: DateTime.now()
    //   ),
    //   new Note(
    //     noteID: "2",
    //     noteTitle: "Note 2",
    //     createdAt: DateTime.now(),
    //     updatedAt: DateTime.now()
    //   ),
    //   new Note(
    //     noteID: "3",
    //     noteTitle: "Note 3",
    //     createdAt: DateTime.now(),
    //     updatedAt: DateTime.now()
    //   ),
    // ];
  }

  //No List means for single object only
  Future<APIResponse<SpecificNote>> getSpecificNote(String noteID){
    return http.get(API+"/notes/"+noteID,headers:headers).then((data){
      if(data.statusCode == 200){
        final jsonData = jsonDecode(data.body);
        final note = SpecificNote.fromJson(jsonData);
        
        return APIResponse<SpecificNote>(data: note);
        }
      return APIResponse<SpecificNote>(error: true,errorMessage: "An error occured");
    }).catchError((error)=>{
      APIResponse<SpecificNote>(error: true , errorMessage:error )
    });
  }

  //NoteInsert parameter here means can use it own things
  //body:json.encode(item.toJson())
  Future<APIResponse<bool>> createNote(NoteInsert item){
    return http.post(API+"/notes" ,headers:headers ,body:json.encode(item.toJson())).then((data){
      if(data.statusCode == 201){

        return APIResponse<bool>(data: true);
        }
      return APIResponse<bool>(error: true,errorMessage: "An error occured");
    }).catchError((error)=>{
      APIResponse<bool>(error: true , errorMessage:error )
    });
  }

  Future<APIResponse<bool>> updateNote(String noteID,NoteInsert item){
    return http.put(API+"/notes/"+ noteID ,headers:headers ,body:json.encode(item.toJson())).then((data){
      if(data.statusCode == 204){

        return APIResponse<bool>(data: true);
        }
      return APIResponse<bool>(error: true,errorMessage: "An error occured");
    }).catchError((error)=>{
      APIResponse<bool>(error: true , errorMessage:error )
    });
  }

  Future<APIResponse<bool>> deleteNote(String noteID){
    return http.delete(API+"/notes/"+ noteID ,headers:headers ).then((data){
      if(data.statusCode == 204){

        return APIResponse<bool>(data: true);
        }
      return APIResponse<bool>(error: true,errorMessage: "An error occured");
    }).catchError((error)=>{
      APIResponse<bool>(error: true , errorMessage:error )
    });
  }
}